#!/usr/bin/env python2
"""
This is an Emulab profile which models the real-world testbed deployed in Ammon, Idaho
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as igext

pc = portal.Context()

pc.defineParameter("osNodeType", "Hardware Type",
                   portal.ParameterType.NODETYPE, "",
                   longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")
pc.defineParameter(name="ovs_name",
                   description="Base name for the edge OVS switches",
                   typ=portal.ParameterType.STRING,
                   defaultValue="ovs")
pc.defineParameter(name="host_name",
                   description="Base name for the host nodes",
                   typ=portal.ParameterType.STRING,
                   defaultValue="host")

params = pc.bindParameters()

image_urn = 'emulab.net'
image_project = 'SafeEdge' # I need to move this to the Safeedge emulab project...
image_os = 'Ubuntu_18.04'
image_tag = 'FRR:3'

router_disk_image = 'urn:publicid:IDN+{urn}+image+{proj}:{os}_{tag}'.format(urn=image_urn, proj=image_project, os=image_os, tag=image_tag)

request = pc.makeRequestRSpec()


def connectNodes(request, node1, node2):
    """
    Build an anonymous LAN to connect two nodes
    """
    node1_name = node1.client_id
    node2_name = node2.client_id

    LAN = request.LAN("{n1}-{n2}".format(n1=node1_name, n2=node2_name))
    node1_interface = node1.addInterface("{n1}-{n2}".format(n1=node1_name, n2=node2_name))
    node2_interface = node2.addInterface("{n2}-{n1}".format(n1=node1_name, n2=node2_name))
    LAN.addInterface(node1_interface)
    LAN.addInterface(node2_interface)


def node_constructor(hardware_type, *args, **kwargs):
    node = request.RawPC(*args, **kwargs)
    node.hardware_type = hardware_type
    return node


# Build Domain A
CB = node_constructor(params.osNodeType, "CB")
CB.disk_image = router_disk_image

N08 = node_constructor(params.osNodeType, "N08")
N08.disk_image = router_disk_image

N02 = node_constructor(params.osNodeType, "N02")
N02.disk_image = router_disk_image

N07 = node_constructor(params.osNodeType, "N07")
N07.disk_image = router_disk_image

N10 = node_constructor(params.osNodeType, "N10")
N10.disk_image = router_disk_image

# Interconnect Domain A
# N08 and N07 are handled implicitly by the other connections
# Connect CB
for node in (N08, N02, N07):
    connectNodes(request, CB, node)

# Connect N9
for node in (N08, N07, N10):
    connectNodes(request, N02, node)

# Connect N5
for node in (N08, N07):
    connectNodes(request, N10, node)

pc.printRequestRSpec(request)
